base_dir=~/.ktech_projects/

mkdir -p ${base_dir}/projects

# Create aliases for every script in each project project
for project_name in $(ls ${base_dir}/projects);
do
    project_path=$(cat ${base_dir}/projects/$project_name)
    for script in $(ls ${project_path}/scripts)
    do
       alias ${project_name}_$script=$project_path/scripts/$script
    done
done
