# Source the project environment variables
source .env

# Remove the project entry
rm -f ~/.ktech_projects/projects/$COMPOSE_PROJECT_NAME
