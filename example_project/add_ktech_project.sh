# Adds the current directory as a docker_project

# Source the projects environment variables
source .env

# Install the ktech-project-alias-manager in the user's root directory
if [ -d ~/.ktech_projects/ ];
then
    mkdir ~/.ktech_projects
    cd ~/.ktech_projects
    curl https://bitbucket.org/katharostech/ktech-project-alias-manager/get/master.tar.gz | tar -xz
    mv katharostech-ktech-project-alias-manager-*/* .
    rm -rf katharostech-ktech-project-alias-manager-*
fi

# For now we use the $COMPOSE_PROJECT_NAME environment variable
# for the name of the project. This variable will exist in most
# if not all katharostech projects. We may or may not want to
# change this later.
echo "$(pwd)" > ~/.ktech_projects/projects/$COMPOSE_PROJECT_NAME
